﻿using System.ComponentModel.Design;

internal class Program
{
    private static void Main(string[] args)
    {
        while (true) 
        {
        Console.WriteLine("Введите число: ");
        int firstValue = int.Parse(Console.ReadLine());
        bool a = firstValue >= 0 & firstValue <= 14;
        bool b = firstValue >= 15 & firstValue <= 35;
        bool c = firstValue >= 36 & firstValue <= 49;
        bool d = firstValue >= 50 & firstValue <= 100;
        if (a == true)
        {
            Console.WriteLine("Число попадает в диапазон 0...14");
        }
        else if (b == true)
        {
            Console.WriteLine("Число попадает в диапазон 15...35");
        }
        else if (c == true)
        {
            Console.WriteLine("Число попадает в диапазон 36...49");
        }
        else if (d == true)
        {
            Console.WriteLine("Число попадает в диапазон 50...100");
        }
        else
            {
                Console.WriteLine("Неизвестный диапазон");
            }
        Console.ReadLine();
        }
    }
}